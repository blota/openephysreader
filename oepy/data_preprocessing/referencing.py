__author__ = 'blota'

"""
Should hold helper functions to do post-hoc referencing. The idea for now is to try common average reference
"""

import numpy as np
import gc
from ..utils import make_list


# from ..utils import progressbar as pgb


def create_ref(data_reader, channels, chunk=30000, verbose=True,
               out_dtype=None, method=np.median):
    """Create a single data line by combining `channels` of `data_reader` using `method`

     `data_reader` is loaded by chunks of `chunk` samples, upcasted to float64, transformed by `method` and
     downcasted to `out_dtype`. If `out_dtype` is None, then same dtype as input is used. `method` can be any
     function that accepts 1D arrays and returns a scalar (np.median and np.mean for instance)

    :param data_reader: anything that points to the data (oe_reader.OpenEphysReader.kwd_#.rec_# for instance)
    :param channels: list of channels
    :param chunk: int
    :param verbose: bool
    :param out_dtype: None or np.dtype
    :return: np.array
    """

    channels = make_list(channels, int)
    npts = data_reader.shape[0]
    calculation_dtype = np.dtype(float)  # dtype used to average

    if out_dtype is None:
        out_dtype = data_reader[0, 0].dtype

    chunk_size = chunk * len(channels) * calculation_dtype.itemsize
    if verbose:
        print('%i chunks of %.2f Mb to load' % (np.ceil(float(npts) / chunk), chunk_size / 1024. / 1024.))
        print('Output will be %.2f Mb' % (float(npts) * out_dtype.itemsize / 1024. / 1024))

    # widgets = ['Creating ref: ', pgb.Percentage(), ' ', pgb.Bar(marker=pgb.RotatingMarker()),
    #            ' ', pgb.ETA()]
    output = np.zeros(npts, dtype=out_dtype)
    n_read = 0
    # pbar = pgb.ProgressBar(widgets=widgets, maxval=npts + 1).start()
    while n_read < npts:
        end = min(npts, n_read + chunk)
        data_chunk = np.asarray(data_reader[n_read:end, channels], dtype=calculation_dtype)
        output[n_read:end] = np.asarray(np.apply_along_axis(method, 1, data_chunk), dtype=out_dtype)
        del data_chunk
        gc.collect()
        n_read = end
    # pbar.update(n_read + 1)
    #
    # pbar.finish()
    return output
